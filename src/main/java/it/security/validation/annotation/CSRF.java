package it.security.validation.annotation;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author Andrea
 *
 */
@Target({ METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CSRF {

	/**
	 * Controllo abilitato per qualsiasi richiesta verso l'applicazione.
	 * 
	 * @return
	 */
	boolean request() default true;

	/**
	 * Genera csrf header in fase di response.
	 * 
	 * @return
	 */
	boolean response() default true;
}
