package it.security.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.bind.annotation.RequestBody;

import it.security.constant.ExceptionConstants;
import it.security.exception.ApiException;
import it.security.validation.annotation.SecureGenericCheck;
import it.security.validation.annotation.SecureGenericCheckObject;
import it.security.validation.annotation.SecurePatternCheck;
import it.security.validation.pattern.PatternSecurityCheck;

/**
 * 
 * @author Andrea
 *
 */
public class SecureUtil {

	/**
	 * tipi di controlli effettuabili
	 * 
	 * @author Andrea
	 *
	 */
	public enum SecurityCheckType {
		GENERIC, OBJECT, PATTERN, CSRF
	}

	/**
	 * @param toEncode
	 * @return encoded64 string
	 */

	public String encodeBase64(String toEncode) {
		byte[] data = toEncode.getBytes();
		return new Base64().encodeToString(data);
	}

	/**
	 * @param encoded
	 * @return decoded64 string
	 */
	public static String decodeBase64(String encoded) {
		return new String(Base64.decodeBase64(encoded));
	}

	/**
	 * Controllo generico sul valore di un determinato valore.
	 * 
	 * Utile quando devono essere effettuati controlli sui parametri che vengono
	 * passati ai controller per evitare che non vi siano presenti eventuali
	 * caratteri compromissivi (es: che potrebbero causare SQL injection).
	 * 
	 * @param arg
	 * @param charBlackList
	 * @return
	 */
	public static boolean genericSecurityCheck(Object arg, String charBlackList) {

		if (arg == null) {
			return true;
		}

		// controllo su caratteri non ammessi
		if (arg instanceof String) {
			String converted = (String) arg;
			arg = converted.toLowerCase();

			final String value = converted;

			// cercati i caratteri
			String[] split = charBlackList.split(";");
			String foundCharacter = Arrays.asList(split).stream().filter(blackChar -> value.contains(blackChar))
					.findAny().orElse(null);

			// "true" solamente se non vi sono caratteri particolari nel "value"
			return !(value.substring(0, 1).equals("'")
					|| value.substring(value.length() - 1, value.length()).equals("'")) && (foundCharacter == null);
		}

		return true;

	}

	/**
	 * Vengono controllate tutte le proprietà di un oggetto applicando per ognuna di
	 * esse il <b>genericSecurityCheck</b>.
	 * 
	 * Utile quando dev'essere effettuato un controllo su un body che viene passato
	 * in una request da un controller. In questo caso, per ogni proprietà
	 * valorizzata dell'oggetto, viene applicato il controllo.
	 * 
	 * @param object
	 * @param charBlackList
	 * @return
	 * @throws Exception
	 */
	public static boolean genericSecurityCheckObject(Object object, String charBlackList) throws Exception {
		Method[] methods = object.getClass().getMethods();
		for (Method method : methods) {
			// soltanto i getter
			// TODO da eslcudere il getCLass() e i getter sulle password
			// in modo più furbo
			if (method.getName().toLowerCase().substring(0, 3).equals("get")
					&& !method.getName().toLowerCase().equals("getclass")
					&& !method.getName().toLowerCase().contains("password")) {
				return genericSecurityCheck(method.invoke(object, (Object[]) null), charBlackList);
			}
		}
		return true;
	}

	/**
	 * Viene effettuato un controllo custom per l'oggetto passato nel body di una
	 * request (non viene applicato il <b>genericSecurityCheck</b>).
	 * 
	 * È necessario esplicitare un oggetto <b>PatternSecurityCheck</b>
	 * nell'annotation <b>SecurePatternCheck</b> che contenga il riferimento della
	 * classe addetta a questo controllo.
	 * 
	 * @param annotations
	 * @param objectToCheck
	 * @return
	 * @throws Exception
	 */
	public static boolean patternSecurityCheck(Annotation[] annotations, Object objectToCheck) throws Exception {
		// viene presa la prima annotation @SecurePatternCheck (ce ne dev'essere
		// solamente una)
		for (Annotation annotation : annotations) {
			if (annotation.annotationType().equals(SecurePatternCheck.class)) {
				SecurePatternCheck patternWrapper = (SecurePatternCheck) annotation;
				Class<?> pattern = patternWrapper.pattern();
				// istanziata la classe che deve effettuare il controllo
				PatternSecurityCheck newInstance = (PatternSecurityCheck) pattern.newInstance();
				// esecuzione del controllo
				return newInstance.isPatternMatch(objectToCheck);
			}
		}
		return true;
	}

	/**
	 * LOGICA COSTRUZIONE CSRF HEADER:
	 * 
	 * 1. La stringa del contenuto per il quale si vuole generare l'header, viene
	 * trasformata in MD5 (scelto perchè è ciò che garantisce la maggiore unicità
	 * per il suo output: "The chance of you winning the lottery would probably be
	 * greater than the chance of having two hashes the same come out of that
	 * function");
	 * 
	 * 2. Quest'ultimo viene concatenato ad una stringa a piacere (salt) nota ad
	 * entrambe le parti;
	 * 
	 * 2.1. L’unione delle due viene trasformata in Base64 per comodità (risultante
	 * successivamente utilizzata come chiave di cifratura);
	 * 
	 * 3. Criptato il contenuto del body utilizzando algoritmo AES con padding e
	 * chiave di cifratura AES/CBC/PKCS5Padding;
	 * 
	 * 4. In quanto l'algoritmo AES dà in output anche caratteri non utf-8, per
	 * semplificare la trasmissione su protocollo HTTP, viene tutto trasformato in
	 * Base64;
	 * 
	 * 5. Trasformazione del Base64 in stringa;
	 * 
	 * @param salt
	 * @param response
	 * @return
	 */
	public static String buildCsrfValue(String salt, String body) {
		try {

			// 1
			String key = new String(MessageDigest.getInstance("MD5").digest(body.getBytes()));

			// 2
			String saltyKey = key + salt;

			// 2.1
			String keyFinal = new SecureUtil().encodeBase64(saltyKey);

			// 3
			byte[] encrypted = new CryptoUtil().setup(keyFinal, "AES", "AES/CBC/PKCS5Padding").encrypt(body);

			// 4
			byte[] encryptedEncoded = new Base64().encode(encrypted);

			// 5
			return new String(encryptedEncoded);

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Ritorna il tipo di controllo da effettuare per un determinato parametro in
	 * funzione della/e sue annotations.
	 * 
	 * @param parameterAnnotations
	 * @return
	 */
	public static SecurityCheckType getCheckType(Annotation annotation) {
		if (annotation.annotationType().equals(SecureGenericCheck.class)) {
			return SecurityCheckType.GENERIC;
		}
		if (annotation.annotationType().equals(SecureGenericCheckObject.class)) {
			return SecurityCheckType.OBJECT;
		}
		if (annotation.annotationType().equals(SecurePatternCheck.class)) {
			return SecurityCheckType.PATTERN;
		}
		if (annotation.annotationType().equals(RequestBody.class)) {
			return SecurityCheckType.CSRF;
		}
		return null;
	}

	/**
	 * Effettua l'opportuno controllo in funzione delle annotation presenti sul
	 * parametro
	 * 
	 * @param parameterAnnotations
	 * @param arg
	 */
	public static void check(Annotation[] parameterAnnotations, Object arg, String charBlackList) {
		for (Annotation annotation : parameterAnnotations) {
			SecurityCheckType checkType = getCheckType(annotation);
			if (checkType != null) {
				switch (checkType) {
				case GENERIC:
					boolean genericSecurityCheck = SecureUtil.genericSecurityCheck(arg, charBlackList);
					if (!genericSecurityCheck) {
						throw new ApiException(ExceptionConstants.INVALID_INPUT_VALUE_CODE);
					}
					break;
				case OBJECT:
					try {
						boolean genericSecurityCheckObject = SecureUtil.genericSecurityCheckObject(arg, charBlackList);
						if (!genericSecurityCheckObject) {
							throw new ApiException(ExceptionConstants.INVALID_INPUT_VALUE_CODE);
						}
					} catch (Exception e) {
						// TODO gestire in modo non bloccate l'eccezione in quanto non dovuta da un
						// input invalido.
					}
					break;
				case PATTERN:
					try {
						boolean specificSecurityCheck = SecureUtil.patternSecurityCheck(parameterAnnotations, arg);
						if (!specificSecurityCheck) {
							throw new ApiException(ExceptionConstants.INVALID_INPUT_VALUE_CODE);
						}
					} catch (Exception e) {
						throw new ApiException(ExceptionConstants.INVALID_INPUT_VALUE_CODE);
					}
					break;
				default:
					break;
				}
			}
		}
	}

}
