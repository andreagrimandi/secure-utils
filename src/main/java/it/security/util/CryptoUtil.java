package it.security.util;

import java.nio.charset.Charset;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * @author andre
 *
 */
public class CryptoUtil {

	private static String keyStringDes = "p4$$3nC0";

	/**
	 * 
	 */
	private String key;

	/**
	 * 
	 */
	private String alg;

	/**
	 * 
	 */
	private String cipher;

	/**
	 * 
	 * @param keyString
	 * @param cipher
	 * @throws Exception
	 */
	public CryptoUtil setup(String keyString, String alg, String cipher) throws Exception {
		this.key = keyString;
		this.alg = alg;
		this.cipher = cipher;
		return this;
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public byte[] encrypt(String input) throws Exception {
		byte[] keyBytes = this.key.getBytes(Charset.forName("UTF-8"));
		SecretKeySpec key = new SecretKeySpec(keyBytes, this.alg);
		Cipher cipher = Cipher.getInstance(this.cipher);
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[16]));
		byte[] inputBytes = input.getBytes(Charset.forName("UTF-8"));
		return cipher.doFinal(inputBytes);
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public String decrypt(byte[] input) throws Exception {
		byte[] keyBytes = this.key.getBytes(Charset.forName("UTF-8"));
		SecretKeySpec key = new SecretKeySpec(keyBytes, this.alg);
		Cipher cipher = Cipher.getInstance(this.cipher);
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
		byte[] inputDecryptedBytes = cipher.doFinal(input);
		return new String(inputDecryptedBytes, Charset.forName("UTF-8"));
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static String encryptDes(String input) throws Exception {
		SecretKeySpec key = new SecretKeySpec(keyStringDes.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] raw = cipher.doFinal(input.getBytes());
		String strCipher = new Base64().encodeToString(raw);

		return strCipher;
	}

	/**
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static String decryptDes(String input) throws Exception {
		SecretKeySpec key = new SecretKeySpec(keyStringDes.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] raw = Base64.decodeBase64(input);
		byte[] stringBytes = cipher.doFinal(raw);
		String plain = new String(stringBytes);

		return plain;
	}

}
