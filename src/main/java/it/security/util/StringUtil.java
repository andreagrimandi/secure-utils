package it.security.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author andre
 *
 */
public class StringUtil {

	/**
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isVoid(String string) {
		return (string == null || "".equals(string.trim()));
	}

	/**
	 * 
	 * @param body
	 * @return
	 */
	public static String toJson(Object body) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(body);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
