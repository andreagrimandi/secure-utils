package it.security.util;

import java.security.MessageDigest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

public class CSRFUtil {

	/**
	 * 
	 * @param request
	 * @param headerName
	 * @return
	 */
	public static Cookie getHeaderFromRequest(HttpServletRequest request, String headerName) {
		Cookie[] cookies = request.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals(headerName)) {
				return cookie;
			}
		}
		return null;
	}

	/**
	 * LOGICA COSTRUZIONE CSRF HEADER:
	 * 
	 * 1. La stringa del contenuto per il quale si vuole generare l'header, viene
	 * trasformata in MD5 (scelto perchè è ciò che garantisce la maggiore unicità
	 * per il suo output: "The chance of you winning the lottery would probably be
	 * greater than the chance of having two hashes the same come out of that
	 * function");
	 * 
	 * 2. Quest'ultimo viene concatenato ad una stringa a piacere (salt) nota ad
	 * entrambe le parti;
	 * 
	 * 2.1. L’unione delle due viene trasformata in Base64 per comodità (risultante
	 * successivamente utilizzata come chiave di cifratura);
	 * 
	 * 3. Criptato il contenuto del body utilizzando algoritmo AES con padding e
	 * chiave di cifratura AES/CBC/PKCS5Padding;
	 * 
	 * 4. In quanto l'algoritmo AES dà in output anche caratteri non utf-8, per
	 * semplificare la trasmissione su protocollo HTTP, viene tutto trasformato in
	 * Base64;
	 * 
	 * 5. Trasformazione del Base64 in stringa;
	 * 
	 * @param salt
	 * @param response
	 * @return
	 */
	public static String buildCsrfValue(String salt, String body) {
		try {

			// 1
			String key = new String(MessageDigest.getInstance("MD5").digest(body.getBytes()));

			// 2
			String saltyKey = key + salt;

			// 2.1
			String keyFinal = new SecureUtil().encodeBase64(saltyKey);

			// 3
			byte[] encrypted = new CryptoUtil().setup(keyFinal, "AES", "AES/CBC/PKCS5Padding").encrypt(body);

			// 4
			byte[] encryptedEncoded = new Base64().encode(encrypted);

			// 5
			return new String(encryptedEncoded);

		} catch (Exception e) {
			return null;
		}
	}

}
