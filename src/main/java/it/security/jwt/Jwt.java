package it.security.jwt;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.security.constant.ExceptionConstants;
import it.security.exception.ApplicationException;
import it.security.jwt.model.AuthenticateResponse;
import it.security.util.CryptoUtil;

/**
 * 
 * @author andre
 *
 */
public class Jwt {

	/**
	 * 
	 */
	private String key;

	/**
	 * 
	 */
	private String alg;

	/**
	 * 
	 */
	private String cipher;

	/**
	 * 
	 */
	private Map<String, String> args;

	/**
	 * 
	 * @param key
	 * @param alg
	 * @param cipher
	 */
	public Jwt(String key, String alg, String cipher) {
		super();
		this.key = key;
		this.alg = alg;
		this.cipher = cipher;
	}

	/**
	 * @param args
	 */
	public Jwt setArgs(Map<String, String> args) {
		this.args = args;
		return this;
	}

	/**
	 * 
	 * @param args
	 * @return
	 */
	public Jwt setArg(String key, String value) {
		this.args.put(key, value);
		return this;
	}

	/**
	 * 
	 * @param userIdentifier
	 * @param password
	 * @param secret
	 * @param jwtExpirationDays
	 * @return
	 * @throws Exception
	 */
	public AuthenticateResponse generateAuthenticateResponse(String userIdentifier, String password, String secret,
			int jwtExpirationDays) throws Exception {
		return this.generateAuthenticateResponse(userIdentifier, password, secret, jwtExpirationDays, 0, null);
	}

	/**
	 * 
	 * @param userIdentifier
	 * @param password
	 * @param secret
	 * @param jwtExpirationDays
	 * @param refreshJwtExpirationDays
	 * @return
	 * @throws Exception
	 */
	public AuthenticateResponse generateAuthenticateResponse(String userIdentifier, String password, String secret,
			int jwtExpirationDays, int refreshJwtExpirationDays) throws Exception {
		return this.generateAuthenticateResponse(userIdentifier, password, secret, jwtExpirationDays,
				refreshJwtExpirationDays, null);
	}

	/**
	 * 
	 * @param userIdentifier
	 * @param password
	 * @param secret
	 * @param jwtExpirationDays
	 * @param refreshJwtExpirationDays
	 * @param args
	 * @return
	 * @throws Exception
	 */
	public AuthenticateResponse generateAuthenticateResponse(String userIdentifier, String password, String secret,
			int jwtExpirationDays, int refreshJwtExpirationDays, Map<String, String> args) throws Exception {
		/**
		 * Do note that for signed tokens this information, though protected against
		 * tampering, is readable by anyone. Do not put secret information in the
		 * payload or header elements of a JWT unless it is encrypted.
		 */
		userIdentifier = new String(new CryptoUtil().setup(key, alg, cipher).encrypt(userIdentifier));

		AuthenticateResponse response = new AuthenticateResponse();

		// data di scadenza per il token di autenticazione
		Date tokenExpirationDate = Date
				.from(LocalDateTime.now().plusDays(jwtExpirationDays).atZone(ZoneId.systemDefault()).toInstant());

		response.setAccessToken(this.generateToken(userIdentifier, password, secret, tokenExpirationDate, false, args));
		response.setExpiresOn(tokenExpirationDate);
		response.setUserIdentifier(new CryptoUtil().setup(key, alg, cipher).decrypt(userIdentifier.getBytes()));

		if (refreshJwtExpirationDays > 0) {
			// data di scadenza per il token di refresh
			Date refrTokenExpirationDate = Date.from(
					LocalDateTime.now().plusDays(refreshJwtExpirationDays).atZone(ZoneId.systemDefault()).toInstant());
			response.setRefreshToken(
					this.generateToken(userIdentifier, password, secret, refrTokenExpirationDate, false, args));
		}

		return response;
	}

	/**
	 * Generates a JWT token containing username as subject, and userId and role as
	 * additional claims. These properties are taken from the specified User object.
	 * 
	 * @param identifier
	 * @param password
	 * @param secret
	 * @param expiration
	 * @param isRefreshToken
	 * @return
	 * @throws Exception
	 */
	private String generateToken(String identifier, String password, String secret, Date expiration,
			boolean isRefreshToken, Map<String, String> args) throws Exception {
		Claims claims = Jwts.claims().setSubject(identifier);
		claims.put("expiration", expiration);

		// discriminating whether is a refresh token or not
		claims.put("refresh", isRefreshToken);

		if (password != null) {
			claims.put("password", password);
		}

		// other optional parameters that may be inside JWT token
		for (Map.Entry<String, String> entry : args.entrySet()) {
			claims.put(entry.getKey(), entry.getValue());
		}

		String compact = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).compact();
		byte[] encrypt = this.key != null ? new CryptoUtil().setup(this.key, this.alg, this.cipher).encrypt(compact)
				: compact.getBytes();
		return new Base64().encodeAsString(encrypt);
	}

	/**
	 * 
	 * @param token
	 * @return
	 */
	public ObjectNode decodeToken(String token) throws Exception {
		// remove the first part of the token because it's useless
		ObjectNode tokenData = new ObjectMapper().readValue(token.substring(15), ObjectNode.class);

		// token must not be expired
		if (Jwt.isTokenExpired(tokenData))
			throw new ApplicationException(ExceptionConstants.EXPIRED_JWT_TOKEN_CODE);

		return tokenData;
	}

	/**
	 * 
	 * @param token
	 * @return
	 */
	public static boolean isTokenExpired(ObjectNode token) {
		return !new Date(token.get("expiration").asLong()).after(new Date());
	}
}