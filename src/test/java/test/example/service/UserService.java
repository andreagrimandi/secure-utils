package test.example.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import test.example.model.User;

/**
 * 
 * @author andre
 *
 */
@Service
public class UserService {

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public User getUserById(Integer id) {
		return null;
	}

	/**
	 * 
	 * @param email
	 * @return
	 */
	@Transactional
	public User getUserByUsername(String email) {
		return null;
	}

}
