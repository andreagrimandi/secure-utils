package test.example.main;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.security.constant.ExceptionConstants;
import it.security.exception.ApplicationException;
import it.security.jwt.Jwt;
import it.security.jwt.JwtAuthentication;
import it.security.jwt.model.AuthenticateResponse;
import it.security.jwt.model.UserCredentials;
import it.security.jwt.model.UserToAuthenticate;

/**
 * 
 * @author andre
 *
 */
public class MainTest {

	/**
	 * 
	 */
	private static String secret = "4CF2F8C0B4F74DA54E55D22AC1BEA541C91D43643F14B41A7B9553126C6C9B1F";

	/**
	 * 
	 */
	private static String key = "VkYp3s6v9y$B&E)H@McQfTjWmZq4t7w!";

	/**
	 * 
	 */
	private static String alg = "AES";

	/**
	 * 
	 */
	private static String cipher = "AES/CBC/PKCS5Padding";

	/**
	 * 
	 */
	private static int expirationDaysToken = 10;

	/**
	 * 
	 */
	private static int expirationDaysRefreshToken = 10;

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		// the authenticationr equest
		UserCredentials userCredentials = new UserCredentials("test@test.it", "123456");

		// the actual user that must be authenticated (usually taken locally based on
		// "username" of "UserCredentials")
		UserToAuthenticate userToAuthenticate = new UserToAuthenticate("id_utente", "test@test.it", "123456");

		AuthenticateResponse authenticate = new JwtAuthentication(secret, key, alg, cipher, expirationDaysToken,
				expirationDaysRefreshToken).authenticate(userCredentials, userToAuthenticate);

		String accessToken = authenticate.getAccessToken();

		ObjectNode tokenData = new Jwt(key, alg, cipher).decodeToken(accessToken);

		if (!tokenData.get("refresh").asBoolean()) {
			if (Jwt.isTokenExpired(tokenData)) {
				throw new ApplicationException(ExceptionConstants.EXPIRED_JWT_TOKEN_CODE);
			}
		} else {
			throw new ApplicationException(ExceptionConstants.INVALID_JWT_TOKEN_CODE);
		}

		if (!JwtAuthentication.isTokenValid(tokenData, userToAuthenticate)) {
			throw new ApplicationException(ExceptionConstants.WRONG_PASSWORD_CODE);
		}

	}

}
