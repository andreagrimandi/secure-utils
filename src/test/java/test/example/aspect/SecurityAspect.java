package test.example.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import it.security.constant.ExceptionConstants;
import it.security.exception.ApiException;
import it.security.util.SecureUtil;
import it.security.util.StringUtil;
import it.security.validation.annotation.CSRF;

/**
 * Aspect adibito ai controlli sui parametri di input degli endpoint nei
 * controllers.
 * 
 * Vengono intercettate tutte le funzioni, all'interno dei controllers che hanno
 * come annotation @Secure. Per ognuna di queste, viene effettuato un controllo
 * sugli eventuali parametri che rpesentano le annotations @SecureGenericCheck
 * o @SecureGenericCheckObject
 * 
 * @author Andrea
 *
 */
@Aspect
public class SecurityAspect {

	@Value("${b2c.security.csrf.enabled}")
	private String csrfEnabled;

	@Value("${b2c.security.csrf.headerName}")
	private String csrfHeaderName;

	@Value("${b2c.security.csrf.salt}")
	private String salt;

	@Value("${b2c.security.charBlackList}")
	private String charBlackList;

	/**
	 * Eseguito come controllo di sicurezza sulla conformità dle contenuto dei
	 * aprametri per evitare sql injection/altro
	 */
	@Pointcut("execution(* *(..)) && @annotation(it.security.validation.annotation.Secure)")
	public void genericSecurityCheck() {
	}

	/**
	 * Si riferisce a tutti gli endpoint con l'annotation @CSRF
	 */
	@Pointcut("execution(* *(..)) && @annotation(it.security.validation.annotation.CSRF)")
	public void csrfCheck() {
	}

	// -----------------------------------------------------------------------------------------------------

	/**
	 * qualsiasi metodo di qualsiasi classe che abbia come annotation @Secure passa
	 * da questa funzione la quale procede con l'analizzare tutti i parametri
	 * presenti.
	 * 
	 * @param joinPoint
	 */
	@Before("genericSecurityCheck()")
	public void applyGenericSecurityCheck(JoinPoint joinPoint) {
		// tutte le annotations presenti nei parametri
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		Annotation[][] parameterAnnotations = method.getParameterAnnotations();

		// i valori dei vari parametri
		Object[] args = joinPoint.getArgs();

		// il controllo viene effettuato su ogni parametro.
		// La funzione check determina quale tipo di controllo effettuare
		for (int i = 0; i < args.length; i++) {
			if (args[i] != null) {
				SecureUtil.check(parameterAnnotations[i], args[i], charBlackList);
			}
		}

	}

	/**
	 * Chiamato per ogni funzione che presenta un <b>@CSRF</b>
	 * 
	 * @param joinPoint
	 */
	@Before("csrfCheck()")
	public void applyCsrfCheck(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		CSRF annotation = method.getAnnotation(CSRF.class);

		// solo se il metodo ha l'annotation @CSRF presente e il controllo csrf è
		// abilitato
		if (new Boolean(csrfEnabled).booleanValue() && annotation != null && annotation.request()) {
			Annotation[][] parameterAnnotations = method.getParameterAnnotations();

			Object[] args = joinPoint.getArgs();

			for (int i = 0; i < args.length; i++) {

				// nel caso il parametro abbia l'annotation @RequestBody, se CSRF attivo, viene
				// effettuato quel controllo
				Annotation csrfAnnotation = Arrays.asList(parameterAnnotations[i]).stream().filter(
						predicate -> SecureUtil.getCheckType(predicate).equals(SecureUtil.SecurityCheckType.CSRF))
						.findFirst().orElse(null);

				if (csrfAnnotation != null) {
					// context holder attuale della richiesta in corso
					HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
							.currentRequestAttributes()).getRequest();

					// contenuto dell'header
					String csrfHeader = request.getHeader(csrfHeaderName);
					// riprodotto il contenuto dell'header utilizzando la request attuale
					String csrfValue = SecureUtil.buildCsrfValue(salt, StringUtil.toJson(args[i]));
					// se non corrispondono, significa che o l'header o il corpo della request sono
					// stati manomessi
					if (!csrfHeader.equals(csrfValue)) {
						throw new ApiException(ExceptionConstants.INVALID_INPUT_VALUE_CODE);
					}
				}

			}
		}
	}

}
