package test.example.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import it.security.util.CSRFUtil;
import it.security.util.StringUtil;
import it.security.validation.annotation.CSRF;

@ControllerAdvice
public class CSRFHandler implements ResponseBodyAdvice<Object> {

	@Value("${security.csrf.enabled}")
	private String csrfEnabled;

	@Value("${security.csrf.headerName}")
	private String csrfHeaderName;

	@Value("${security.csrf.salt}")
	private String salt;

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {

		CSRF annotation = returnType.getMethod().getAnnotation(CSRF.class);

		// controllo effettuato solamente se abilitato da configurazione e sul
		// rispettivo endpoint
		if (new Boolean(csrfEnabled).booleanValue() && annotation != null && annotation.response()) {
			// generato il CSRF token da inviare assieme alla response in modo tale che il
			// client potrà fare le opportune verifiche
			String csrfValue = CSRFUtil.buildCsrfValue(salt, StringUtil.toJson(body));
			response.getHeaders().add(csrfHeaderName, csrfValue);
		}

		return body;
	}

}
